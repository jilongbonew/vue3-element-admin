
const ChatUiClient = (function(){
    // todo 这里需要改成需要嵌入的系统的路径 注意默认写一个路径传参? 例如 https://xxx.com?dialog=true;
    const __domain = 'http://localhost:3000/chatUi';
    const __baseIframeUrl =  `${__domain}/html/chatUi.html?dialog=true`;
    function ChatUi() {
    }
    let _sessionId,_appId,_appName;
    /**
     * 设置使用聊天的系统会话
     * */
    ChatUi.setSessionId = function (sessionId){
        _sessionId = sessionId;
    }
    /**
     * 设置使用聊天的系统id和名称
     * */
    ChatUi.setAppInfo = function (appId,appName){
       _appId=appId || '';
       _appName=appName || '';
    }
    function loadCss(){
        const new_element = document.createElement('link');
        new_element.setAttribute('rel', 'stylesheet');
        new_element.setAttribute('href', 'http://localhost:3000/chatUi/css/chatUi.css');
        document.body.appendChild(new_element);
    }

    function loadHtml(){
        document.writeln("<div id=\'J_xiaomi_dialog\' style=\'z-index: 999999; right: 40px; bottom: 50px;display:none\'>");
        document.writeln("            <div class=\'J_weak\'>");
        document.writeln("                <img src=\'//gw.alicdn.com/tfs/TB1ZY1ycUD1gK0jSZFGXXbd3FXa-142-142.png\' class=\'alime-avatar\'>");
        document.writeln("                <span class=\'alime-text\'>点我聊天</span>");
        document.writeln("            </div>");
        document.writeln("        <div id=\'J_Dialog\' class=\'J_Dialog alicare-dialog-undefined\'>");
        document.writeln("            <div class=\'alicare-dialog-wrapper\'>");
        document.writeln("                <div class=\'dialog-fake-mask\'></div>");
        document.writeln("                <div class=\'alicare-fake-header alicare-draggable\'>");
        document.writeln("                    <a class=\'alicare-im-close\'>");
        document.writeln("                        <img src=\'//gw.alicdn.com/tfs/TB1lWlNOkvoK1RjSZPfXXXPKFXa-29-29.svg\'>");
        document.writeln("                    </a>");
        document.writeln("                </div>");
        document.writeln("                <iframe id=\'chatIframe\' allowfullscreen=\'true\' class=\'alicare-frame\' name=\'alicare-dialog\' src=\'about:blank\' frameborder=\'0\'></iframe>");
        document.writeln("            </div>");
        document.writeln("        </div>");
        document.writeln("    </div>");
    }
    function getSessionId(){
            return _sessionId;
    }
    function getIframeUrl(){
            const session = getSessionId();
            const targetOrigin = window.location.host
            return `${__baseIframeUrl}&sessionId=${session}&appId=${_appId}&appName=${_appName}&targetOrigin=${targetOrigin}`
    }
    function dialogClick(){
        document.getElementById("chatIframe").src = getIframeUrl();
        // 获取iframe的URL
        const JDialog = document.getElementById("J_Dialog");
        if(JDialog.style.display==="none" || JDialog.style.display==='')
        {
            JDialog.style.display = "block";   /*****可见****/
        }
        else
        {
            JDialog.style.display = "none";    /*****不可见****/
        }

    }
    window.onload=function (){
        loadCss();
        const dialog = document.getElementById('J_xiaomi_dialog');
        setTimeout(()=>{
            dialog.style.display = "block";   /*****可见****/
        })
        dialog.onclick= dialogClick;
    }
    loadHtml();
	/**
	 * 测试VUE的操作流程
	 * */
	function flowHtml(){
		const menuItems = document.getElementsByClassName('el-menu-item');
		const menuItemsSize = menuItems.length;
		let menuItem;
		for (let i = 0; i < menuItemsSize; i++) {
			if(menuItems[i].textContent === '用户管理'){
				menuItem=menuItems[i];
			}
		}
		menuItem.click();
	}

	function flowVue(textContent){
		const menuItems = document.getElementsByClassName('jlq');
		const menuItemsSize = menuItems.length;
		let menuItem;
		for (let i = 0; i < menuItemsSize; i++) {
			if(menuItems[i].textContent === textContent){
				menuItem=menuItems[i];
			}
		}
		// menuItem.click();
		menuItem.__vueParentComponent.devtoolsRawSetupState.push();
		fromVue();
	}

	function fromVue(){
		const systemUser = document.getElementById('systemUser');
		if(systemUser){
			systemUser.__vueParentComponent.devtoolsRawSetupState.openDialog()
		}else {
			setTimeout(()=>{
				fromVue();
			},1000)
		}
	}


    window.addEventListener('message',e=>{
		console.log('evalStr===>',e.data);
		// const appVue = document.getElementById("app").__vue_app__;
		flowVue(e.data);
        if(__domain===e.origin ){
            console.log('evalStr===>',e.data);
			// const appVue = document.getElementById("app").__vue_app__;
			flowVue();
            // 执行子系统传递过来的js脚本
            // eval(e.data);
            // console.log(e.origin) //子页面URL，这里是http://b.index.com
            // console.log(e.source) // 子页面window对象，全等于iframe.contentWindow
            // console.log(e.data) //子页面发送的消息
        }
    },false)
    return ChatUi;
})();

